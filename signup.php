<?php
require_once "app/init.php";
if(!empty($_POST))
{
	$email = $_POST['email'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$rules = [
		"email"=>[
			"required"=>true,
			"maxlength"=>255,
			"unique"=>"users",
			"email"=>true
		],
		"username"=>[
			"required"=>true,
			"minlength"=>2,
			"maxlength"=>20,
			"unique"=>"users"
		],
		"password"=>[
			"required"=>true,
			"minlength"=>8,
			"maxlength"=>255
		]
	];
	$validation = $validator->check($_POST, $rules);
	if($validation->fails())
	{
		echo "<pre>" . print_r($validation->errors()->all()) . "</pre";
	}
	else
	{
		// echo "All validation is successful, you can insert record in database!";
		$created = $auth->create([
			'email'=>$email,
			'password'=>$password,
			'username'=>$username
		]);
		if($created)
		{
			header("Location: index.php");
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign up</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
	<form action="signup.php" method="POST">
		<fieldset>
			<legend>Sign Up</legend>
			<label>
				Email:
				<input type="text" name="email">
			</label>
			<label>
				Username:
				<input type="text" name="username">
			</label>
			<label>
				Password:
				<input type="password" name="password">
			</label>
			<input type="submit" value="Sign Up">
		</fieldset>
	</form>
</body>
</html>