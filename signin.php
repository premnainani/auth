<?php
require_once "app/init.php";
if(!empty($_POST))
{
	$username = $_POST['username'];
	$password = $_POST['password'];
	$rememberMe = $_POST['remember_me'];

	$validator->check($_POST, [
		'username'=>[
			'required'=>true,
		],
		'password'=>[
			'required'=>true,
			'minlength'=>8
		]
	]);
	if($validator->fails())
	{
		echo "<pre>", print_r($validator->errors()->all()), "</pre>";
	}
	else
	{
		//log the user in
		$signin = $auth->signin([
			'username'=>$username,
			'password'=>$password
		]);
		if($signin)
		{
			header("Location: index.php");
		}
		// remember me?
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign In</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
	<form action="signin.php" method="POST">
		<fieldset>
			<legend>Sign In</legend>
			<label>
				Username:
				<input type="text" name="username">
			</label>
			<label>
				Password:
				<input type="password" name="password">
			</label>
			<input type="checkbox" value="remember_me">
			<input type="submit" value="Sign Up">
		</fieldset>
	</form>
</body>
</html>