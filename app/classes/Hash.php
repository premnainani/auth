<?php
class Hash
{
    public function make($plainText)
    {
        return password_hash($plainText, PASSWORD_BCRYPT, ['cost'=>10]);
    }
    public function verify($plain, $hash)
    {
        return password_verify($plain, $hash);
    }
}