<?php
class Validator
{
    protected $database;
    protected $errorHandler;
    protected $rules = ["required", "minlength", "maxlength", "unique", "email"];
    protected $messages = [
        "required" => "This :field field is required",
        "minlength" => "The :field field must be a minimum of :satisfier characters",
        "maxlength" => "The :field field must be a maximum of :satisfier characters",
        "email" => "This is not a valid email address",
        "unique" => "That :field is already taken"
    ];

    /**
     * Validator Constructor
     * @param Database $database
     * @param ErrorHandler $errorHandler
     */
    public function __construct(Database $database, ErrorHandler $errorHandler)
    {
        $this->database = $database;
        $this->errorHandler = $errorHandler;
    }
    public function check($items, $rules)
    {
        foreach ($items as $item=>$value)
        {
            if(in_array($item, array_keys($rules)))
            {
                $this->validate([
                    'field'=>$item,
                    'value'=>$value,
                    'rules'=>$rules[$item]
                ]);
            }
        }
        return $this;
    }

    public function fails()
    {
        return $this->errorHandler->hasErrors();
    }
    public function errors()
    {
        return $this->errorHandler;
    } 
    private function validate($item)
    {
        /**
         * $item['field'] -> contains the column name which has to be tested for the validation
         * $item['value'] -> contains the value which was inserted by the user in the form
         * $item['rules'] -> It is array of rules to be applied for the specific 'field'
         */
        $field = $item['field'];
        $value = $item['value'];
        foreach ($item['rules'] as $rule=>$satisfier)
        {
            if(!call_user_func_array([$this, $rule], [$field, $value, $satisfier]))
            {
                // store the error in the error handler
                $this->errorHandler->addError(str_replace([':field', ':satisfier'],[$field, $satisfier], $this->messages[$rule]), $field);
            }
        }
        // die(var_dump($item));
    }

    private function required($field, $value, $satisfier)
    {
        return !empty(trim($value));
    }
    private function minlength($field, $value, $satisfier)
    {
        return mb_strlen($value)>=$satisfier;
    }
    private function maxlength($field, $value, $satisfier)
    {
        return mb_strlen($value)<=$satisfier;
    }
    private function unique($field, $value, $satisfier)
    {
        // Here $satisfier will become the name of table!
        // $field will become the name of column
        // $value should be unique under the above table and column
        return !$this->database->table($satisfier)->exists([$field=>$value]);
    }
    private function email($field, $value, $satisfier)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}